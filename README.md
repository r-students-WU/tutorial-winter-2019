# Repository for the VW-SozÖk-Seep R-Tutorial Winter 2019

## Dates & Location

Time: Always 18:00 to 20:00 (6pm to 8pm)

- Mon, Oct 14 2019
    - TC.2.03 Seminar room (60) 	

- Mon, Oct 21 2019
    - TC.2.03 Seminar room (60) 

- Mon, Oct 28 2019
    - TC.2.03 Seminar room (60) 

- Mon, Nov 04 2019
    - TC.4.01 Seminar room (60) 



## Software downloads

***You don't have to install anything beforehands - We'll set up R at the beginning of the first session***

### R

- Debian/Ubuntu: `sudo apt install r-base`
    - also see `https://gitlab.com/r-students-WU/DotfilesAndOtherNonRcode/tree/master/ubuntu18_04`

- RHEL/CentOS/Fedora: `sudo yum install R`

- (open)SUSE: https://cran.r-project.org/bin/linux/suse/

- MacOS: https://cran.r-project.org/bin/macosx/

- Windows: https://cran.r-project.org/bin/windows/

### R-Studio

https://www.rstudio.com/products/rstudio/download/#download

### Git

https://git-scm.com/downloads

### Other Resources

https://imsmwu.github.io/MRDA2019/_book/index.html



